--
-- Table structure
--

DROP DATABASE IF EXISTS metrodb;

create database metrodb;

USE metrodb;

DROP TABLE IF EXISTS LINEAS;

CREATE TABLE LINEAS (
  id_linea int(11) AUTO_INCREMENT,
  nombre varchar(45),
  en_funcionamiento int(11),
  aforo int(11),
  tipo varchar(45),
  PRIMARY KEY (id_linea)
) ENGINE=INNODB; 

DROP TABLE IF EXISTS PARADAS;

CREATE TABLE PARADAS (
    id_parada int(11) AUTO_INCREMENT,
    nombre varchar(45),
    cp int(5),
    PRIMARY KEY (id_parada)
) ENGINE=INNODB;

DROP TABLE IF EXISTS LINEAS_PARADAS;

CREATE TABLE LINEAS_PARADAS (
    id_linea int(11),
    id_parada int(11),
    PRIMARY KEY (id_linea, id_parada),
    FOREIGN KEY (id_linea) REFERENCES LINEAS (id_linea) ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (id_parada) REFERENCES PARADAS (id_parada) ON UPDATE CASCADE ON DELETE RESTRICT    
) ENGINE=INNODB;

LOCK TABLES LINEAS WRITE;

INSERT INTO LINEAS VALUES(1,
                          '1',
                          '5',
                          '200',
                          'metro');

INSERT INTO LINEAS VALUES(2,
                          '2',
                          '4',
                          '250',
                          'metro');

INSERT INTO LINEAS VALUES(3,
                          '3',
                          '6',
                          '150',
                          'metro');

INSERT INTO LINEAS VALUES(4,
                          '4',
                          '3',
                          '200',
                          'tram');

INSERT INTO LINEAS VALUES(5,
                          '5',
                          '8',
                          '300',
                          'metro');

INSERT INTO LINEAS VALUES(6,
                          '6',
                          '4',
                          '250',
                          'tram');

INSERT INTO LINEAS VALUES(7,
                          '7',
                          '10',
                          '150',
                          'metro');

INSERT INTO LINEAS VALUES(8,
                          '8',
                          '5',
                          '250',
                          'tram');

INSERT INTO LINEAS VALUES(9,
                          '9',
                          '6',
                          '225',
                          'metro');

INSERT INTO LINEAS VALUES(10,
                          '10',
                          '5',
                          '200',
                          'tram');

UNLOCK TABLES;

LOCK TABLES PARADAS WRITE;

INSERT INTO PARADAS VALUES (1,'Villanueva de Castellon','46270'),(2,'L`Alcudia','46250'),(3,'Picassent','46220'),(4,'Torrent','46900'),(5,'Betera','46117'),
                           (6,'Torrent Avinguda','46900'),(7,'Valencia Sud','46014'),(8,'Campanar','46015'),(9,'Paterna','46980'),(10,'Lliria','46160'),
                           (11,'Aeropuerto','46940'),(12,'Av Del Cid','46018'),(13,'Benimaclet','46020'),(14,'Alboraya-Peris Arago','46120'),(15,'Rafelbunyol','46138'),
                           (16,'Dr Lluch','46011'),(17,'Tarongers Ernest Lluch','46022'),(18,'Reus','46009'),(19,'V. A. Estelles','46100'),(20,'Mas Del Rosari','46980'),
                           (21,'Aeropuerto','46940'),(22,'Quart de Poblet','46930'),(23,'Av Del Cid','46018'),(24,'Xativa','46007'),(25,'Maritim Serreria','46022'),
                           (26,'Maritim Serreria','46022'),(27,'Dr Lluch','46011'),(28,'Tarongers Ernest Lluch','46022'),(29,'Benimaclet','46020'),(30,'Tossal del Rei','46019'),
                           (31,'Torrent Avinguda','46900'),(32,'Valencia Sud','46014'),(33,'Bailen','46007'),(34,'Colon','46004'),(35,'Maritim Serreria','46022'),   
                           (36,'Maritim Serreria','46022'),(37,'Francesc Chubells','46011'),(38,'Grau Canyamelar','46011'),(39,'Marina Reial Joan Carles I','46011'),
                           (40,'Riba Roja De Turia','46190'),(41,'Quart de Poblet','46930'),(42,'Av Del Cid','46018'),(43,'Benimaclet','46020'),(44,'Alboraya-Peris Arago','46120'),
                           (45,'Alacant','46022'),(46,'Amado Granell Montolivet','46006'),(47,'Ciutat Arts I Ciencies Justicia','46013'),(48,'Oceanografic','46013'),(49,'Natzaret','46024');

UNLOCK TABLES;

LOCK TABLES LINEAS_PARADAS WRITE;

INSERT INTO LINEAS_PARADAS VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(2,6),(2,7),(2,8),(2,9),(2,10),(3,11),(3,12),(3,13),(3,14),(3,15),(4,16),(4,17),(4,18),(4,19),(4,20),(5,21),(5,22),(5,23),(5,24),(5,25),(6,26),(6,27),(6,28),(6,29),(6,30),(7,31),(7,32),(7,33),(7,34),(7,35),(8,36),(8,37),(8,38),(8,39),(9,40),(9,41),(9,42),(9,43),(9,44),(10,45),(10,46),(10,47),(10,48),(10,49);

UNLOCK TABLES;

